#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=36
#SBATCH --time=0-08:00:00
#SBATCH --mem-per-cpu=1G
#SBATCH --job-name=nori_rendering
#SBATCH --output=nori_log.out

module load cmake/3.25.0
module load gcc/9.3.0

echo "Running Nori Rendering with $SLURM_NTASKS tasks, $SLURM_CPUS_PER_TASK cores per task, $SLURM_MEM_PER_CPU memory per core"

cd $HOME/norirayenginetilman/
for scene in scenes/euler_render/*.xml; do
    echo "Rendering $scene"
    ./build/nori --no-gui $scene
done

echo "Done rendering"

exit 0
