#include <nori/medium.h>
#include <nori/phasefunction.h>
#include <nori/perlinnoisemap.h>

NORI_NAMESPACE_BEGIN

class PerlinMedium : public Medium {
public:
    PerlinMedium(const PropertyList &props) { 
        m_sigmaA = props.getColor("sigmaA", Color3f(0.0f));
        m_sigmaS = props.getColor("sigmaS", Color3f(0.0f));
        m_emission   = props.getColor("emission", Color3f(0.0f));
        m_gridsize   = props.getFloat("gridSize", 1.0f);

        perlinmap = new PerlinNoiseMap(m_gridsize);
    }

    virtual Color3f samplePerlinMap(const Point3f &p, Color3f maxval) const {
        float v = perlinmap->sample(p) * 0.5f + 0.5f;
        Color3f transmitance = v * maxval;
        return transmitance.hasNaN() ? Color3f(1.0f): transmitance;
    }

    virtual Color3f evalTransmittance(const Ray3f &ray) const {
        Color3f sigmaA = samplePerlinMap(ray.o, m_sigmaA);
        Color3f sigmaS = samplePerlinMap(ray.o, m_sigmaS);
        Color3f sigmaT = sigmaA + sigmaS;
        return Color3f(exp(-sigmaT * ray.maxt));
    };

    virtual Color3f evalAbsorption(const Ray3f &ray) const {
        return samplePerlinMap(ray.o, m_sigmaA);
    };

    virtual Color3f evalScattering(const Ray3f &ray) const {
        return samplePerlinMap(ray.o, m_sigmaS);
    };

    virtual Color3f evalExtinction(const Ray3f &ray) const {
        Color3f sigmaA = samplePerlinMap(ray.o, m_sigmaA);
        Color3f sigmaS = samplePerlinMap(ray.o, m_sigmaS);
        return sigmaA + sigmaS;
    };

    virtual Color3f evalAlbedo(const Ray3f &ray) const {
        Color3f sigmaA = samplePerlinMap(ray.o, m_sigmaA);
        Color3f sigmaS = samplePerlinMap(ray.o, m_sigmaS);
        Color3f sigmaT = sigmaA + sigmaS;
        Color3f result = sigmaS / sigmaT;
        return result.hasNaN() ? Color3f(1.0f): result;
    }

    virtual Color3f evalEmission(const Ray3f &ray) const { 
        Color3f emission = samplePerlinMap(ray.o, m_emission);
        return emission.hasNaN() ? Color3f(0.0f): emission;; 
    };

    virtual float pdf(const Vector3f &wo, const Vector3f &wi) const {
        if (m_phaseFunction) {
            PhaseQueryRecord pRec(wo, wi);
            return m_phaseFunction->pdf(pRec);
        } else
            return 1.0f;
    };

    virtual std::string toString() const {
        return tfm::format(
            "PerlinMedium[\n"
            "  sigmaA = %s,\n"
            "  sigmaS = %s,\n"
            "  emission = %s,\n"
            "  gridsize = %f,\n"
            "  phase = %s\n"
            "]",
            m_sigmaA.toString(),
            m_sigmaS.toString(),
            m_emission.toString(),
            m_gridsize,
            m_phaseFunction ? indent(m_phaseFunction->toString()) : std::string("null")
        );
    }

private:
    Color3f m_sigmaA;
    Color3f m_sigmaS;
    Color3f m_emission;
    float m_gridsize;

    PerlinNoiseMap *perlinmap;
};

NORI_REGISTER_CLASS(PerlinMedium, "perlinmedium");
NORI_NAMESPACE_END