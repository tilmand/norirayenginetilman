#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
 
NORI_NAMESPACE_BEGIN
 
class RecursiveMC : public Integrator {
public:
    RecursiveMC(const PropertyList &props) { 
        m_maxDepth = props.getInteger("maxDepth", 5);
        m_indSamples = props.getInteger("indirectSamples", 1);
    }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        return RecursiveLi(scene, sampler, ray, m_maxDepth, false);
    }

    Color3f RecursiveLi(const Scene *scene, Sampler *sampler, const Ray3f &ray, int depth, bool prev_diffuse) const {
        // Stop recursion
        if (depth == 0)
            return Color3f(0.0f);

        // Find the surface that is visible in the requested direction
        Intersection its;
        if (!scene->rayIntersect(ray, its))
            return Color3f(0.0f);
 
        // Calculate emission from the surface itself
        Color3f Le(0.0f);
        if (its.mesh->isEmitter()) {
            EmitterQueryRecord selflRec(ray.o, its.p, its.shFrame.n);
            Le = its.mesh->getEmitter()->eval(selflRec);
        }

        Color3f Li(0.0f);
        /// Sample all lights
        for (auto light : scene->getLights()) {
            // Sample light
            EmitterQueryRecord lRec(its.p);
            Color3f lightEmitted = light->sample(lRec, sampler->next2D());
            if (lightEmitted.isZero() || scene->rayIntersect(lRec.shadowRay))
                continue;

            // Evaluate the BSDF model
            BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(lRec.wi), ESolidAngle);
            bRec.uv = its.uv;
            bRec.p = its.p;
            Color3f bsdf = its.mesh->getBSDF()->eval(bRec);

            // Add contribution
            Li += bsdf * lightEmitted;
        }

        /// Sample indirect light
        for (int i = 0; i < m_indSamples; i++) {
            // Sample BSDF
            BSDFQueryRecord bRec(its.toLocal(-ray.d));
            bRec.uv = its.uv;
            bRec.p = its.p;
            Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());

            // Add contribution
            Li += bsdf * 
                RecursiveLi(
                    scene, 
                    sampler, 
                    Ray3f(its.p, its.toWorld(bRec.wo)), 
                    depth - 1, 
                    its.mesh->getBSDF()->isDiffuse()
                ) /
                m_indSamples;
        }

        // Return result without emission to prevent double counting
        if (prev_diffuse)
            return Li;
        else
            return Le + Li;
    }

 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "RecursiveMC"
        );
    }

protected:
    int m_maxDepth;
    int m_indSamples;
};
 
NORI_REGISTER_CLASS(RecursiveMC, "recursive_mc");
NORI_NAMESPACE_END