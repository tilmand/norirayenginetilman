
#include <nori/warp.h>
#include <nori/phasefunction.h>
#include <nori/frame.h>
#include <nori/common.h>

NORI_NAMESPACE_BEGIN

class Isotropic: public PhaseFunction {
public:
    Isotropic(const PropertyList &props) {
    }

    virtual float sample(PhaseQueryRecord &pRec, const Point2f &sample) const {
        pRec.wo = Warp::squareToUniformSphere(sample);
        return 1.0f / (4 * M_PI);
    }

    virtual float pdf(const PhaseQueryRecord &pRec) const {
        return 1.0f / (4 * M_PI);
    }

    virtual std::string toString() const {
        return tfm::format(
            "Isotropic[\n"
            "  g = %f"
            "]",    
            0.0f
        );
    };
};

NORI_REGISTER_CLASS(Isotropic, "isotropic");
NORI_NAMESPACE_END