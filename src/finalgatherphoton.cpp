/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/emitter.h>
#include <nori/bsdf.h>
#include <nori/scene.h>
#include <nori/photon.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

class FinalGatherPhoton : public Integrator {
public:
    /// Photon map data structure
    typedef PointKDTree<Photon> PhotonMap;

    FinalGatherPhoton(const PropertyList &props) {
        /* Lookup parameters */
        m_photonCount  = props.getInteger("photonCount", 10000000);
        m_photonRadius = props.getFloat("photonRadius", 0.0f /* Default: automatic */);
        m_gatherSamples = props.getInteger("gatherSamples", 1000);
    }

    virtual void preprocess(const Scene *scene) override {
        cout << "Gathering " << m_photonCount << " photons .. ";
        cout.flush();

        /* Create a sample generator for the preprocess step */
        Sampler *sampler = static_cast<Sampler *>(
            NoriObjectFactory::createInstance("independent", PropertyList()));

        /* Allocate memory for the photon map */
        m_photonMap = std::unique_ptr<PhotonMap>(new PhotonMap());
        m_photonMap->reserve(m_photonCount);

		/* Estimate a default photon radius */
		if (m_photonRadius == 0)
			m_photonRadius = scene->getBoundingBox().getExtents().norm() / 500.0f;

	

		/* How to add a photon?
		 * m_photonMap->push_back(Photon(
		 *	Point3f(0, 0, 0),  // Position
		 *	Vector3f(0, 0, 1), // Direction
		 *	Color3f(1, 2, 3)   // Power
		 * ));
		 */

		// put your code to trace photons here

        for (int i = 0; i < m_photonCount; i++) {
            Ray3f path;
            Color3f W = scene->sampleRandomPhoton(path, sampler->next1D(), sampler->next2D(), sampler->next2D());
            int burnIn = 3;
            while (true)
            {
                Intersection its;
                if (!scene->rayIntersect(path, its))
                    break;

                // Detect is surface is diffuse and deposite photon
                if (its.mesh->getBSDF()->isDiffuse())
                    m_photonMap->push_back(Photon(its.p, -path.d, W));

                // Russian roulette
                float successProb = std::min(W.maxCoeff(), 0.99f);
                if (sampler->next1D() >= successProb)
                    if (burnIn <= 0)
                        break;
                    else
                        burnIn--;
                else
                    W /= successProb;

                // Sample BSDF
                BSDFQueryRecord bRec(its.toLocal(-path.d));
                bRec.uv = its.uv;
                bRec.p = its.p;
                Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
                if (bsdf.isZero())
                    break;

                // Update throughput
                W *= bsdf;

                // Update path
                path = Ray3f(its.p, its.toWorld(bRec.wo));
            }
        }

		/* Build the photon map */
        m_photonMap->build();
    }

    virtual Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &_ray) const override {
    	
		/* How to find photons?
		 * std::vector<uint32_t> results;
		 * m_photonMap->search(Point3f(0, 0, 0), // lookup position
		 *                     m_photonRadius,   // search radius
		 *                     results);
		 *
		 * for (uint32_t i : results) {
		 *    const Photon &photon = (*m_photonMap)[i];
		 *    cout << "Found photon!" << endl;
		 *    cout << " Position  : " << photon.getPosition().toString() << endl;
		 *    cout << " Power     : " << photon.getPower().toString() << endl;
		 *    cout << " Direction : " << photon.getDirection().toString() << endl;
		 * }
		 */

		// put your code for path tracing with photon gathering here

        Color3f Li(0.0f);
        Ray3f ray(_ray);
        Color3f throughput(1.0f);
        float numLights = scene->getLights().size();
        bool hitDiffuse = false;
        while (true)
        {
            Intersection its;
            if (!scene->rayIntersect(ray, its))
                break;
                    
            if (its.mesh->isEmitter()) {
                EmitterQueryRecord selflRec(ray.o, its.p, its.shFrame.n);
                Color3f Le = its.mesh->getEmitter()->eval(selflRec);
                Li += throughput * Le;
            }

            // Check BSDF for diffuse surface
            if (its.mesh->getBSDF()->isDiffuse()) {
                Color3f L_samples = Color3f(0.0f);
                if (hitDiffuse) {
                    //find the closest photons
                    std::vector<uint32_t> results;
                    m_photonMap->search(its.p, m_photonRadius, results);
                    //sum the power of the photons
                    Color3f L_(0.0f);
                    for (uint32_t i : results) {
                        const Photon &photon = (*m_photonMap)[i];
                        L_ += photon.getPower();
                    }
                    //add the contribution
                    L_samples += throughput * L_ / (M_PI * m_photonRadius * m_photonRadius * m_gatherSamples * m_photonCount);
                    break;
                }
                else {
                    hitDiffuse = true;
                                /*
                Color3f L_samples = Color3f(0.0f);
                for(int i = 0; i < m_gatherSamples; i++){
                //sample directions on the hemisphere above the intersection point
                Vector3f wi = Warp::squareToCosineHemisphere(sampler->next2D());
                //convert to world space
                wi = its.toWorld(wi);
                //create new ray
                Ray3f newRay(its.p, wi);
                Intersection newIts;
                if (!scene->rayIntersect(newRay, newIts)) 
                    continue;
                if(newIts.mesh->getBSDF()->isDiffuse()){
                    
                    //find the closest photons
                    std::vector<uint32_t> results;
                    m_photonMap->search(newIts.p, m_photonRadius, results);
                    //sum the power of the photons
                    Color3f L_(0.0f);
                    for (uint32_t i : results) {
                        const Photon &photon = (*m_photonMap)[i];
                        L_ += photon.getPower();
                    }
                    //add the contribution
                    L_samples += throughput * L_ / (M_PI * m_photonRadius * m_photonRadius * m_gatherSamples * m_photonCount);
                    continue;
                }
                */
                }
            }

            // Russian roulette
            float successProb = std::min(throughput.maxCoeff(), 0.99f);
            if (sampler->next1D() >= successProb)
                break;
            throughput /= successProb;

            // Sample BSDF
            BSDFQueryRecord bRec(its.toLocal(-ray.d));
            bRec.uv = its.uv;
            bRec.p = its.p;
            Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
            if (bsdf.isZero())
                break;

            // Update throughput
            throughput *= bsdf;

            // Update path
            ray = Ray3f(its.p, its.toWorld(bRec.wo));
        }

		return Li;
    }

    virtual std::string toString() const override {
        return tfm::format(
            "PhotonMapper[\n"
            "  photonCount = %i,\n"
            "  photonRadius = %f\n"
            "]",
            m_photonCount,
            m_photonRadius
        );
    }
private:
    /* 
     * Important: m_photonCount is the total number of photons deposited in the photon map,
     * NOT the number of emitted photons. You will need to keep track of those yourself.
     */ 
    int m_photonCount;
    float m_photonRadius;
    int m_gatherSamples;
    std::unique_ptr<PhotonMap> m_photonMap;
};

NORI_REGISTER_CLASS(FinalGatherPhoton, "finalgatherphoton");
NORI_NAMESPACE_END
