/// implement rayleigh phase function

#include <nori/phasefunction.h>
#include <nori/common.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

class Rayleigh: public PhaseFunction {
public:
    Rayleigh(const PropertyList &props) {}

    virtual float sample(PhaseQueryRecord &pRec, const Point2f &sample) const {
        // sample wo according to the phase function
        float cosTheta = 2 * sample.x() - 1;
        float sinTheta = sqrt(1 - cosTheta * cosTheta);
        float phi = 2 * M_PI * sample.y();
        pRec.wo = Vector3f(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta);
        return pdf(pRec);
    }

    virtual float pdf(const PhaseQueryRecord &pRec) const {
        float cosTheta = pRec.wo.dot(pRec.wi);
        return INV_PI * 3 / 16 * (1 + cosTheta * cosTheta);
    }

    virtual std::string toString() const {
        return "Rayleigh[]";
    }

};

NORI_REGISTER_CLASS(Rayleigh, "rayleigh");
NORI_NAMESPACE_END