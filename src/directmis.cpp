#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
 
NORI_NAMESPACE_BEGIN
 
class DirectIntegratorMis : public Integrator {
public:
    DirectIntegratorMis(const PropertyList &props) { }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        // Find the surface that is visible in the requested direction
        Intersection its;
        if (!scene->rayIntersect(ray, its))
            return Color3f(0.0f);
 
        // Calculate emission from the surface itself
        Color3f Li(0.0f);
        if (its.mesh->isEmitter()) {
            EmitterQueryRecord selflRec(ray.o, its.p, its.shFrame.n);
            Li = its.mesh->getEmitter()->eval(selflRec);
        }


        /// Sample Emission
        // Sample a random light source
        const Emitter *light = scene->getRandomEmitter(sampler->next1D());
        std::vector<Emitter *> lights = scene->getLights();

        // Sample a point on the light source
        EmitterQueryRecord lRecEms(its.p);
        Color3f lightEmittedEms = light->sample(lRecEms, sampler->next2D());
        float pdfEmsOfEms = lRecEms.pdf / (float)lights.size();
        if (scene->rayIntersect(lRecEms.shadowRay))
            lightEmittedEms = Color3f(0.0f);

        // Evaluate the BSDF model
        BSDFQueryRecord bRecEms(its.toLocal(-ray.d), its.toLocal(lRecEms.wi), ESolidAngle);
        bRecEms.uv = its.uv;
        bRecEms.p = its.p;
        Color3f bsdfEms = its.mesh->getBSDF()->eval(bRecEms);
        float pdfMatsOfEms = its.mesh->getBSDF()->pdf(bRecEms);

        Color3f EmissionSample = bsdfEms * lightEmittedEms * Frame::cosTheta(its.toLocal(lRecEms.wi)) * (float)lights.size();


        /// Sample Matterial
        // Sample BSDF
        BSDFQueryRecord bRecMats(its.toLocal(-ray.d));
        bRecMats.uv = its.uv;
        bRecMats.p = its.p;
        Color3f bsdfMats = its.mesh->getBSDF()->sample(bRecMats, sampler->next2D());
        float pdfMatsOfMats = its.mesh->getBSDF()->pdf(bRecMats);

        // Evaluate Light
        Ray3f shadowRay(its.p, its.toWorld(bRecMats.wo));
        Intersection shadowIts;
        float pdfEmsOfMats = 0.0f;
        Color3f lightEmittedMats(0.0f);
        if (scene->rayIntersect(shadowRay, shadowIts) && shadowIts.mesh->isEmitter()) {
            EmitterQueryRecord lRecMats(its.p, shadowIts.p, shadowIts.shFrame.n);
            lightEmittedMats = shadowIts.mesh->getEmitter()->eval(lRecMats);
            pdfEmsOfMats = shadowIts.mesh->getEmitter()->pdf(lRecMats) / (float)lights.size();
        }

        Color3f BRDFSample = bsdfMats * lightEmittedMats;

        // MIS
        float wEmission = pdfEmsOfEms / (pdfEmsOfEms + pdfMatsOfEms);
        float wBSDF = pdfMatsOfMats / (pdfMatsOfMats + pdfEmsOfMats);
        
        Color3f contributionEms = wEmission > 0 ? (Color3f) (wEmission * EmissionSample): Color3f(0.0f);
        Color3f contributionMats = wBSDF > 0 ? (Color3f) (wBSDF * BRDFSample): Color3f(0.0f);
        return Li + contributionEms + contributionMats;
    }

 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "DirectIntegratorMis"
        );
    }
};
 
NORI_REGISTER_CLASS(DirectIntegratorMis, "direct_mis");
NORI_NAMESPACE_END