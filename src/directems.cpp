#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
 
NORI_NAMESPACE_BEGIN
 
class DirectIntegratorEms : public Integrator {
public:
    DirectIntegratorEms(const PropertyList &props) { }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        // Find the surface that is visible in the requested direction
        Intersection its;
        if (!scene->rayIntersect(ray, its))
            return Color3f(0.0f);
 
        // Calculate emission from the surface itself
        Color3f Li(0.0f);
        if (its.mesh->isEmitter()) {
            EmitterQueryRecord selflRec(ray.o, its.p, its.shFrame.n);
            Li = its.mesh->getEmitter()->eval(selflRec);
        }

        // Sample a random light source
        const Emitter *light = scene->getRandomEmitter(sampler->next1D());
        std::vector<Emitter *> lights = scene->getLights();

        // Sample a point on the light source
        EmitterQueryRecord lRec(its.p);
        Color3f lightEmitted = light->sample(lRec, sampler->next2D());
        if (lightEmitted.isZero() || scene->rayIntersect(lRec.shadowRay))
            return Li;

        // Evaluate the BSDF model
        BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(lRec.wi), ESolidAngle);
        bRec.uv = its.uv;
        bRec.p = its.p;
        Color3f bsdf = its.mesh->getBSDF()->eval(bRec);

        // Caclulate integral
        return Li + bsdf * lightEmitted * Frame::cosTheta(its.toLocal(lRec.wi)) * (float)lights.size();
    }
 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "DirectIntegratorEms"
        );
    }
};
 
NORI_REGISTER_CLASS(DirectIntegratorEms, "direct_ems");
NORI_NAMESPACE_END