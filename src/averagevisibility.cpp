#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
 
NORI_NAMESPACE_BEGIN
 
class AverageVisibilityIntegrator : public Integrator {
public:
    AverageVisibilityIntegrator(const PropertyList &props) {
        rayLength = props.getFloat("length", 1.f);
    }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        /* Find the surface that is visible in the requested direction */
        Intersection its;
        if (!scene->rayIntersect(ray, its))
            return Color3f(1.0f);

        Vector3f random_direction = Warp::sampleUniformHemisphere(sampler, its.shFrame.n);
        Ray3f random_ray(its.p, random_direction, Epsilon, rayLength);
        Intersection random_its;
        if (!scene->rayIntersect(random_ray, random_its))
            return Color3f(1.0f);
        else
            return Color3f(0.0f);
    }
 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "AverageVisibility[\n"
            "  rayLength = \"%s\"\n"
            "]",
            rayLength
        );
    }
protected:
    float rayLength;
};
 
NORI_REGISTER_CLASS(AverageVisibilityIntegrator, "av");
NORI_NAMESPACE_END