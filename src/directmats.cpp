#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
 
NORI_NAMESPACE_BEGIN
 
class DirectIntegratorMats : public Integrator {
public:
    DirectIntegratorMats(const PropertyList &props) { }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        // Find the surface that is visible in the requested direction
        Intersection its;
        if (!scene->rayIntersect(ray, its))
            return Color3f(0.0f);
 
        // Calculate emission from the surface itself
        Color3f Li;
        if (its.mesh->isEmitter()) {
            EmitterQueryRecord selflRec(ray.o, its.p, its.shFrame.n);
            Li = its.mesh->getEmitter()->eval(selflRec);
        } else {
            Li = Color3f(0.0f);
        }
        
        // Sample BSDF
        BSDFQueryRecord bRec(its.toLocal(-ray.d));
        bRec.uv = its.uv;
        bRec.p = its.p;
        Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());

        // Evaluate Light
        Ray3f shadowRay(its.p, its.toWorld(bRec.wo), Epsilon, std::numeric_limits<float>::infinity());
        Intersection shadowIts;
        if (!scene->rayIntersect(shadowRay, shadowIts) || !shadowIts.mesh->isEmitter())
            return Li;

        EmitterQueryRecord lRec(its.p, shadowIts.p, shadowIts.shFrame.n);
        Color3f lightEmitted = shadowIts.mesh->getEmitter()->eval(lRec);
        if (lightEmitted.isZero())
            return Li;

        return Li + bsdf * lightEmitted;
    }
 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "DirectIntegratorMats"
        );
    }
};
 
NORI_REGISTER_CLASS(DirectIntegratorMats, "direct_mats");
NORI_NAMESPACE_END