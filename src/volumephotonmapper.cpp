/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/emitter.h>
#include <nori/bsdf.h>
#include <nori/scene.h>
#include <nori/photon.h>
#include <nori/medium.h>
#include <nori/phasefunction.h>

NORI_NAMESPACE_BEGIN

class VolumetricPhotonMapper : public Integrator {
public:
    /// Photon map data structure
    typedef PointKDTree<Photon> PhotonMap;

    VolumetricPhotonMapper(const PropertyList &props) {
        /* Lookup parameters */
        m_photonCount  = props.getInteger("photonCount", 1000000);
        m_photonRadius = props.getFloat("photonRadius", 0.0f /* Default: automatic */);
        m_marchStep    = props.getFloat("marchStep", 0.1f);
    }

    virtual void preprocess(const Scene *scene) override {
        cout << "Gathering " << m_photonCount << " photons .. ";
        cout.flush();

        /* Create a sample generator for the preprocess step */
        Sampler *sampler = static_cast<Sampler *>(
            NoriObjectFactory::createInstance("independent", PropertyList()));

        /* Allocate memory for the surface photon map */
        m_photonMapSurface = std::unique_ptr<PhotonMap>(new PhotonMap());
        m_photonMapSurface->reserve(m_photonCount);

        /* Allocate memory for the volume photon map */
        m_photonMapVolume = std::unique_ptr<PhotonMap>(new PhotonMap());
        m_photonMapVolume->reserve(m_photonCount);

		/* Estimate a default photon radius */
		if (m_photonRadius == 0)
			m_photonRadius = scene->getBoundingBox().getExtents().norm() / 500.0f;

        for (int i = 0; i < m_photonCount; i++) {
            Ray3f path;
            Color3f W = scene->sampleRandomPhoton(path, sampler->next1D(), sampler->next2D(), sampler->next2D());
            const Medium *medium = scene->getMedium();
            while (true)
            {
                Intersection its;
                if (!scene->rayIntersect(path, its))
                    break;

                if (medium) {
                    // perform ray marching
                    Color3f Tr(0.0f);
                    bool stop = false;
                    bool cont = false;
                    float step = m_marchStep;

                    for (float t = 0.0f; t < its.t; t += step)
                    {
                        // update path
                        Ray3f marchRay = Ray3f(path.o + path.d * t, path.d, Epsilon, step);

                        // calculate transmittance
                        Tr += medium->evalTransmittance(marchRay);

                        // Calculate probability of interaction
                        float successProb = std::min(Tr.maxCoeff(), 1.0f);
                        if (sampler->next1D() >= successProb) {
                            // add photon to map
                            m_photonMapVolume->push_back(Photon(marchRay.o, -path.d, W * (1 - Tr)));

                            // decide on interaction type
                            float ratio = (medium->evalScattering(marchRay) / (medium->evalAbsorption(marchRay) + medium->evalScattering(marchRay))).maxCoeff();
                            if (sampler->next1D() < ratio) {
                                // sample phase function
                                Vector3f wi = (-marchRay.d).normalized();
                                Vector3f wo;
                                Color3f phase = medium->samplePhase(wi, wo, sampler->next2D());
                                if (phase.isZero())
                                    break;

                                // update throughput
                                W *= phase;

                                // update path
                                path = Ray3f(marchRay.o, wo);
                                cont = true;
                                break;
                            }
                            else {
                                stop = true;
                                break;
                            }
                        }

                        Tr /= successProb;

                        // jitter step size
                        step = m_marchStep * (1.0f + 0.1f * (sampler->next1D() - 0.5f));
                    }

                    if (stop)
                        break;
                    if (cont)
                        continue;
                }

                // Detect is surface is diffuse and deposite photon
                if (its.mesh->getBSDF()->isDiffuse())
                    m_photonMapSurface->push_back(Photon(its.p, -path.d, W));

                // Russian roulette
                float successProb = std::min(W.maxCoeff(), 0.99f);
                if (sampler->next1D() >= successProb)
                    break;
                else
                    W /= successProb;

                // Sample BSDF
                BSDFQueryRecord bRec(its.toLocal(-path.d));
                bRec.uv = its.uv;
                bRec.p = its.p;
                Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
                if (bRec.inside)
                    medium = its.mesh->getMedium();
                else
                    medium = scene->getMedium();
                if (bsdf.isZero())
                    break;

                // Update throughput
                W *= bsdf;

                // Update path
                path = Ray3f(its.p, its.toWorld(bRec.wo));
            }
        }
        cout << "done." << endl;
        cout.flush();

		/* Build the photon map */
        m_photonMapSurface->build();
        m_photonMapVolume->build();
    }

    virtual Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &_ray) const override {
    	
        Color3f Li(0.0f);
        Ray3f ray(_ray);
        Color3f throughput(1.0f);
        float numLights = scene->getLights().size();
        const Medium *medium = scene->getMedium();

        while (true)
        {
            Intersection its;
            if (!scene->rayIntersect(ray, its))
                break;

            // Sample free path
            if (medium)
            {
                // perform ray marching
                float step = m_marchStep * (1 + 0.1f * (sampler->next1D() - 0.5f));
                Color3f Tr(1.0f);

                for (float t = 0.0f; t < its.t; t += step)
                {
                    // calculate march ray
                    Ray3f marchRay = Ray3f(ray.o + ray.d * t, ray.d, Epsilon, step);

                    // update transmittance
                    Tr *= medium->evalTransmittance(marchRay);

                    // calculate emissive term
                    Color3f Lk = medium->evalEmission(marchRay);

                    // calculate indirect lighting
                    std::vector<uint32_t> results;
                    m_photonMapVolume->search(marchRay.o, m_photonRadius, results);

                    Color3f _Li(0.0f);
                    for (uint32_t i : results) {
                        const Photon &photon = (*m_photonMapVolume)[i];
                        Vector3f wo = (photon.getPosition() - marchRay.o).normalized();
                        _Li += photon.getPower() * medium->evalPhase(wo, photon.getDirection());
                    }
                    _Li *= INV_PI * numLights * 3 / (m_photonRadius * m_photonRadius * m_photonRadius * m_photonCount * 4);
                    Li += throughput * Tr * medium->evalScattering(marchRay) * medium->evalAlbedo(marchRay) * _Li;

                    // jitter step
                    step = m_marchStep * (1 + 0.1f * (sampler->next1D() - 0.5f));
                }

                // add to Li
                throughput *= Tr;
            }
                    
            if (its.mesh->isEmitter()) {
                EmitterQueryRecord selflRec(ray.o, its.p, its.shFrame.n);
                Color3f Le = its.mesh->getEmitter()->eval(selflRec);
                Li += throughput * Le;
            }

            // Check BSDF for diffuse surface
            if (its.mesh->getBSDF()->isDiffuse()) {
                // calculate radiance at point
                std::vector<uint32_t> results;
                m_photonMapSurface->search(its.p, m_photonRadius, results);

                Color3f _L(0.0f);
                for (uint32_t i : results) {
                    const Photon &photon = (*m_photonMapSurface)[i];
                    BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(photon.getDirection()), ESolidAngle);
                    _L += photon.getPower() * its.mesh->getBSDF()->eval(bRec);
                }

                Li += throughput * _L * INV_PI * numLights / (m_photonRadius * m_photonRadius * m_photonCount);
                break;
            }

            // Russian roulette
            float successProb = std::min(throughput.maxCoeff(), 0.99f);
            if (sampler->next1D() >= successProb)
                break;
            throughput /= successProb;

            // Sample BSDF
            BSDFQueryRecord bRec(its.toLocal(-ray.d));
            bRec.uv = its.uv;
            bRec.p = its.p;
            Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
            if (bRec.inside)
                medium = its.mesh->getMedium();
            else 
                medium = scene->getMedium();
            if (bsdf.isZero())
                break;

            // Update throughput
            throughput *= bsdf;

            // Update path
            ray = Ray3f(its.p, its.toWorld(bRec.wo));
        }

		return Li;
    }

    virtual std::string toString() const override {
        return tfm::format(
            "VolumetricPhotonMapper[\n"
            "  photonCount = %i,\n"
            "  photonRadius = %f\n"
            "]",
            m_photonCount,
            m_photonRadius
        );
    }
private:
    /* 
     * Important: m_photonCount is the total number of photons deposited in the photon map,
     * NOT the number of emitted photons. You will need to keep track of those yourself.
     */ 
    int m_photonCount;
    float m_photonRadius;
    float m_marchStep;
    std::unique_ptr<PhotonMap> m_photonMapSurface;
    std::unique_ptr<PhotonMap> m_photonMapVolume;
};

NORI_REGISTER_CLASS(VolumetricPhotonMapper, "vpm");
NORI_NAMESPACE_END
