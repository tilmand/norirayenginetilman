#include <nori/medium.h>
#include <nori/phasefunction.h>

NORI_NAMESPACE_BEGIN

class HomogeneousMedium : public Medium
{
public:
    HomogeneousMedium(const PropertyList &props)
    {
        m_sigmaA = props.getColor("sigmaA", Color3f(0.0f));
        m_sigmaS = props.getColor("sigmaS", Color3f(0.0f));
        m_sigmaT = m_sigmaA + m_sigmaS;
        m_albedo = m_sigmaS / m_sigmaT;
        m_emission = props.getColor("emission", Color3f(0.0f));
    }

    virtual Color3f evalTransmittance(const Ray3f &ray) const
    {
        Color3f transmittance = exp(-m_sigmaT * ray.maxt);
        return transmittance.hasNaN() ? Color3f(1.0f) : transmittance;
    }

    virtual Color3f evalAbsorption(const Ray3f &ray) const
    {
        return m_sigmaA * ray.maxt;
    }

    virtual Color3f evalScattering(const Ray3f &ray) const
    {
        return m_sigmaS * ray.maxt;
    }

    virtual Color3f evalExtinction(const Ray3f &ray) const
    {
        return m_sigmaT * ray.maxt;
    }

    virtual Color3f evalAlbedo(const Ray3f &ray) const { 
        return m_albedo  * ray.maxt; 
    }

    virtual Color3f evalEmission(const Ray3f &ray) const { 
        return m_sigmaA * m_emission * ray.maxt; 
    };

    virtual float pdf(const Vector3f &wo, const Vector3f &wi) const
    {
        if (m_phaseFunction)
        {
            PhaseQueryRecord pRec(wo, wi);
            return m_phaseFunction->pdf(pRec);
        }
        else
            return 1.0f;
    };

    virtual std::string toString() const
    {
        return tfm::format(
            "HomogeneousMedium[\n"
            "  sigmaA = %s,\n"
            "  sigmaS = %s,\n"
            "  sigmaT = %s,\n"
            "  albedo = %s,\n"
            "  emission = %s,\n"
            "  phaseFunction = %s\n"
            "]",
            m_sigmaA.toString(),
            m_sigmaS.toString(),
            m_sigmaT.toString(),
            m_albedo.toString(),
            m_emission.toString(),
            m_phaseFunction->toString());
    }

private:
    Color3f m_sigmaA;
    Color3f m_sigmaS;
    Color3f m_sigmaT;
    Color3f m_albedo;
    Color3f m_emission;
};

NORI_REGISTER_CLASS(HomogeneousMedium, "homogeneous");
NORI_NAMESPACE_END