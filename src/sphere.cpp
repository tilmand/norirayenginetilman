/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Romain Prévost

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/shape.h>
#include <nori/bsdf.h>
#include <nori/emitter.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

class Sphere : public Shape {
public:
    Sphere(const PropertyList & propList) {
        m_position = propList.getPoint3("center", Point3f());
        m_radius = propList.getFloat("radius", 1.f);

        m_bbox.expandBy(m_position - Vector3f(m_radius));
        m_bbox.expandBy(m_position + Vector3f(m_radius));
    }

    virtual BoundingBox3f getBoundingBox(uint32_t index) const override { return m_bbox; }

    virtual Point3f getCentroid(uint32_t index) const override { return m_position; }

    virtual bool rayIntersect(uint32_t index, const Ray3f &ray, float &u, float &v, float &t) const override {
        // Compute potential intersection with the sphere
        Vector3f co = m_position - ray.o;
        float a = co.dot(ray.d);
        if (a < 0 && co.squaredNorm() > m_radius * m_radius)
            return false;

        float b = co.squaredNorm() - a * a;
        if (b > m_radius * m_radius)
            return false;

        // Compute intersection distance
        float c = sqrt(m_radius * m_radius - b);
        float t1 = co.dot(ray.d) - c;
        float t2 = co.dot(ray.d) + c;

        t = std::min(t1, t2);
        if (t < ray.mint || t > ray.maxt) {
            t = std::max(t1, t2);
        
            if (t < ray.mint || t > ray.maxt)
                return false;
        }

        return true;
    }

    virtual void setHitInformation(uint32_t index, const Ray3f &ray, Intersection & its) const override {
        // Compute the intersection point
        its.p = ray.o + ray.d * its.t;

        // Compute the normal
        Vector3f n = (its.p - m_position).normalized();
        its.shFrame = its.geoFrame = Frame(n);

        // Compute the UV coordinates
        its.uv = Point2f(
            0.5f + atan2(n.y(), n.x()) * .5f * INV_PI,
            0.5f - asin(n.z()) * INV_PI
        );
        // its.uv = sphericalCoordinates((its.p - m_position).normalized());
        // its.uv.x() = its.uv.x() * INV_PI * .5f;
        // its.uv.y() = its.uv.y() * INV_PI;
    }

    /*
    virtual void sampleSurface(ShapeQueryRecord & sRec, const Point2f & sample) const override {
        Vector3f q = Warp::squareToUniformSphere(sample);
        sRec.p = m_position + m_radius * q;
        sRec.n = q;
        sRec.pdf = std::pow(1.f/m_radius,2) * Warp::squareToUniformSpherePdf(Vector3f(0.0f,0.0f,1.0f));
    }
    virtual float pdfSurface(const ShapeQueryRecord & sRec) const override {
        return std::pow(1.f/m_radius,2) * Warp::squareToUniformSpherePdf(Vector3f(0.0f,0.0f,1.0f));
    }
    */

    virtual void sampleSurface(ShapeQueryRecord & sRec, const Point2f & sample) const override {
        // Samples a point on the spherical cap
        float cosThetaMax = m_radius / (m_position - sRec.ref).norm();
        Vector3f d = Warp::squareToUniformSphereCap(sample, cosThetaMax);

        // Rotate the sampled direction to the world coordinate system
        Frame frame((sRec.ref - m_position).normalized());
        sRec.n = frame.toWorld(d);
        sRec.p = m_position + sRec.n * m_radius;
        sRec.pdf = std::pow(1.f/m_radius,2) * Warp::squareToUniformSphereCapPdf(d, cosThetaMax);
    }
    virtual float pdfSurface(const ShapeQueryRecord & sRec) const override {
        float cosThetaMax = m_radius / (m_position - sRec.ref).norm();
        Frame frame((sRec.ref - m_position).normalized());
        Vector3f d = frame.toLocal(sRec.n);
        return std::pow(1.f/m_radius,2) * Warp::squareToUniformSphereCapPdf(d, cosThetaMax);
    }

    /*
    virtual void sampleSurface(ShapeQueryRecord & sRec, const Point2f & sample) const override {
        // Sample a point on the spherical cap emenating from sRec.ref
        Vector3f co = m_position - sRec.ref;
        float cosThetaMax = sqrt(1 - m_radius * m_radius / co.squaredNorm());
        Vector3f d = Warp::squareToUniformSphereCap(sample, cosThetaMax);

        // Rotate the sampled direction to the world coordinate system
        Frame frame(co.normalized());
        Vector3f wi = frame.toWorld(d);

        // Computer intersection with the sphere
        float a = co.dot(wi);
        float b = co.squaredNorm() - a * a;
        float c = sqrt(m_radius * m_radius - b);
        float t = co.dot(wi) - c;
        
        sRec.p = sRec.ref + wi * t;
        sRec.n = (sRec.p - m_position).normalized();
        sRec.pdf = Warp::squareToUniformSphereCapPdf(d, cosThetaMax);
    }
    virtual float pdfSurface(const ShapeQueryRecord & sRec) const override {
        Vector3f co = m_position - sRec.p;
        float cosThetaMax = sqrt(1 - m_radius * m_radius / co.squaredNorm());
        Frame frame(co.normalized());

    }
    */


    virtual std::string toString() const override {
        return tfm::format(
                "Sphere[\n"
                "  center = %s,\n"
                "  radius = %f,\n"
                "  bsdf = %s,\n"
                "  emitter = %s\n"
                "  medium = %s\n"
                "]",
                m_position.toString(),
                m_radius,
                m_bsdf ? indent(m_bsdf->toString()) : std::string("null"),
                m_emitter ? indent(m_emitter->toString()) : std::string("null"),
                m_insideMedium ? indent(m_insideMedium->toString()) : std::string("null"));
    }

protected:
    Point3f m_position;
    float m_radius;
};

NORI_REGISTER_CLASS(Sphere, "sphere");
NORI_NAMESPACE_END
