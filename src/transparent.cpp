/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob, Romain Prévost

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/frame.h>

NORI_NAMESPACE_BEGIN

/**
 * \brief Transparent / Lambertian BRDF model
 */
class Transparent : public BSDF {
public:
    Transparent(const PropertyList &propList) { }

    /// Evaluate the BRDF model
    virtual Color3f eval(const BSDFQueryRecord &bRec) const override {
        return Color3f(0.0f);
    }

    /// Compute the density of \ref sample() wrt. solid angles
    virtual float pdf(const BSDFQueryRecord &bRec) const override {
        return 0.0f;
    }

    /// Draw a a sample from the BRDF model
    virtual Color3f sample(BSDFQueryRecord &bRec, const Point2f &sample) const override {
        bRec.wo = -bRec.wi;
        bRec.measure = EDiscrete;
        bRec.eta = 0.0f;
        bRec.inside = Frame::cosTheta(bRec.wi) < 0;

        return Color3f(1.0f);
    }

    /// Return a human-readable summary
    virtual std::string toString() const override {
        return "Transparent[ ]";
    }

    virtual EClassType getClassType() const override { return EBSDF; }

};

NORI_REGISTER_CLASS(Transparent, "transparent");
NORI_NAMESPACE_END
