#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
 
NORI_NAMESPACE_BEGIN
 
class PathIntegratorMats : public Integrator {
public:
    PathIntegratorMats(const PropertyList &props) { }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        Color3f Li(0.0f), throughput(1.0f);
        Ray3f path(ray);
        while (true)
        {
            Intersection its;
            if (!scene->rayIntersect(path, its))
                break;

            // Calculate emission from the surface itself
            if (its.mesh->isEmitter()) {
                EmitterQueryRecord selflRec(path.o, its.p, its.shFrame.n);
                Color3f Le = its.mesh->getEmitter()->eval(selflRec);
                Li += throughput * Le;
            }

            // Russian roulette
            float successProb = std::min(throughput.maxCoeff(), 0.99f);
            if (sampler->next1D() >= successProb)
                break;
            throughput /= successProb;

            // Sample BSDF
            BSDFQueryRecord bRec(its.toLocal(-path.d));
            bRec.uv = its.uv;
            bRec.p = its.p;
            Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
            if (bsdf.isZero())
                break;

            // Update throughput
            throughput *= bsdf;

            // Update path
            path = Ray3f(its.p, its.toWorld(bRec.wo));
        }

        return Li;
    }
 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "PathIntegratorMats"
        );
    }
};
 
NORI_REGISTER_CLASS(PathIntegratorMats, "path_mats");
NORI_NAMESPACE_END