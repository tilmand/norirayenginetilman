#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
 
NORI_NAMESPACE_BEGIN
 
class PathIntegratorMis : public Integrator {
public:
    PathIntegratorMis(const PropertyList &props) { }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        Color3f Li(0.0f), throughput(1.0f);
        Ray3f path = ray;

        std::vector<Emitter *> lights = scene->getLights();
        BSDFQueryRecord bRec(Vector3f(0.0f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, 1.0f), EDiscrete);
        float prevPdf = 1.0f;
        EmitterQueryRecord lRec;

        while (true)
        {
            Intersection its;
            if (!scene->rayIntersect(path, its))
                break;

            // Calculate emission from the surface itself
            if (its.mesh->isEmitter()) {
                lRec = EmitterQueryRecord(path.o, its.p, its.shFrame.n);
                Color3f Le = its.mesh->getEmitter()->eval(lRec);
                if (bRec.measure == EDiscrete) {
                    Li += throughput * Le;
                } else {
                    float pdfEmsofMat = its.mesh->getEmitter()->pdf(lRec) / (float)lights.size();
                    float w_mat = prevPdf / (prevPdf + pdfEmsofMat);
                    Color3f contributionMats = w_mat > 0 ? (Color3f) (w_mat * Le): Color3f(0.0f);
                    Li += throughput * contributionMats;
                }
            }

            // Russian roulette
            float successProb = std::min(throughput.maxCoeff(), 0.99f);
            if (sampler->next1D() >= successProb)
                break;
            throughput /= successProb;

            /// Sample emitters
            // Sample a random point on a random light source
            const Emitter *light = scene->getRandomEmitter(sampler->next1D());
            lRec = EmitterQueryRecord(its.p);
            Color3f lightEmitted = light->sample(lRec, sampler->next2D());

            if (!lightEmitted.isZero() && !scene->rayIntersect(lRec.shadowRay)) {
                // Evaluate the BSDF model
                bRec = BSDFQueryRecord(its.toLocal(-path.d), its.toLocal(lRec.wi), ESolidAngle);
                bRec.uv = its.uv;
                bRec.p = its.p;

                Color3f bsdf = its.mesh->getBSDF()->eval(bRec);
                float pdfEms = lRec.pdf / (float)lights.size();
                float pdfMatofEms = its.mesh->getBSDF()->pdf(bRec);
                float w_em = pdfEms / (pdfEms + pdfMatofEms);
                Color3f EmissionSample = bsdf * lightEmitted * std::abs(Frame::cosTheta(bRec.wo)) * (float)lights.size();
                Color3f contributionEms = w_em > 0 ? (Color3f) (w_em * EmissionSample): Color3f(0.0f);
                Li += throughput * contributionEms;
            }

            /// Sample BSDF
            bRec = BSDFQueryRecord(its.toLocal(-path.d));
            bRec.uv = its.uv;
            bRec.p = its.p;

            Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
            if (bsdf.isZero())
                break;
            prevPdf = its.mesh->getBSDF()->pdf(bRec);
            
            // Update path & throughput
            throughput *= bsdf;
            path = Ray3f(its.p, its.toWorld(bRec.wo));
        }

        return Li;
    }
 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "PathIntegratorMis"
        );
    }
};
 
NORI_REGISTER_CLASS(PathIntegratorMis, "path_mis");
NORI_NAMESPACE_END