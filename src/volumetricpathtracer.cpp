
#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
#include <nori/phasefunction.h>
#include <nori/medium.h>
 
NORI_NAMESPACE_BEGIN
 
class VolumetricIntegrator : public Integrator {
public:
    VolumetricIntegrator(const PropertyList &props) { 
        m_sigmaA = props.getColor("sigmaA", Color3f(0.0f));
        m_sigmaS = props.getColor("sigmaS", Color3f(0.0f));
        m_sigmaT = m_sigmaA + m_sigmaS;
        m_albedo = m_sigmaS / m_sigmaT;
    }

    Color3f samplePF(Ray3f &path, const Intersection &its, Sampler *sampler) const {
        PhaseQueryRecord pRec(its.toLocal(-path.d));
        float pdf = m_phaseFunction->pdf(pRec);
        float phase = m_phaseFunction->sample(pRec, sampler->next2D());
        path = Ray3f(its.p, its.toWorld(pRec.wo));
        return phase * m_albedo / pdf;
    }

    Color3f sampleBSDF(Ray3f &path, const Intersection &its, Sampler *sampler) const {
        BSDFQueryRecord bRec(its.toLocal(-path.d));
        bRec.uv = its.uv;
        bRec.p = its.p;
        Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
        path = Ray3f(its.p, its.toWorld(bRec.wo));
        return bsdf;
    }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        if (!m_phaseFunction)
            throw NoriException("VolumetricIntegrator::Li(): No phase function was provided!");

        Color3f Li(0.0f), throughput(1.0f);
        Ray3f path = ray;

        std::vector<Emitter *> lights = scene->getLights();
        BSDFQueryRecord bRec(Vector3f(0.0f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, 1.0f), EDiscrete);
        PhaseQueryRecord pRec(Vector3f(0.0f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, 1.0f));
        EmitterQueryRecord lRec;
        int burnIn = 3;

        while (true)
        {
            Intersection its;
            if (!scene->rayIntersect(path, its))
                break;


            // Sample emitters
            const Emitter *light = scene->getRandomEmitter(sampler->next1D());

            // Check medium scattering
            float t = -std::log(1 - sampler->next1D()) / m_sigmaT.maxCoeff();
            Color3f absoption(0.0f);
            if (t < its.t) {
                // Sample phase function
                its.p = its.p + t * path.d;
                absoption = samplePF(path, its, sampler);
            } else {
                // Calculate emission from the surface itself
                if (its.mesh->isEmitter()) {
                    lRec = EmitterQueryRecord(path.o, its.p, its.shFrame.n);
                    Color3f Le = its.mesh->getEmitter()->eval(lRec);
                    Li += throughput * Le;
                }

                // Sample BSDF
                absoption = sampleBSDF(path, its, sampler);
            }
            if (absoption.isZero())
                break;

            throughput *= absoption;

            // Russian roulette
            float successProb = std::min(throughput.maxCoeff(), 0.99f);
            if (sampler->next1D() >= successProb)
                if (burnIn-- <= 0)
                    break;
            else
                throughput /= successProb;
        }

        return Li;
    }
 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "VolumetricPathTracer[\n"
            "  sigmaA = %s,\n"
            "  sigmaS = %s,\n"
            "  sigmaT = %s,\n"
            "  albedo = %s\n"
            "]",
            m_sigmaA.toString(),
            m_sigmaS.toString(),
            m_sigmaT.toString(),
            m_albedo.toString()
        );
    }

    virtual void addChild(NoriObject *child) {
        switch (child->getClassType()) {
            case EPhaseFunction: {
                if (m_phaseFunction)
                    throw NoriException("VolumetricIntegrator::addChild(<%s>) tried to register multiple phase functions!",
                        classTypeName(child->getClassType()));
                m_phaseFunction = static_cast<PhaseFunction *>(child);
                break;
            }
            default:
                throw NoriException("VolumetricIntegrator::addChild(<%s>) is not supported!",
                    classTypeName(child->getClassType()));
        }
    }

private:
    Color3f m_sigmaA;
    Color3f m_sigmaS;
    Color3f m_sigmaT;
    Color3f m_albedo;
    PhaseFunction *m_phaseFunction = nullptr;
};
 
NORI_REGISTER_CLASS(VolumetricIntegrator, "vpt");
NORI_NAMESPACE_END