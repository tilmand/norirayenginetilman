/// implement the Schlick phase function

#include <nori/phasefunction.h>
#include <nori/common.h>

NORI_NAMESPACE_BEGIN

class Schlick: public PhaseFunction {
public:
    Schlick(const PropertyList &props) {
        m_g = props.getFloat("g", 0.0f);
    }

    virtual float sample(PhaseQueryRecord &pRec, const Point2f &sample) const {
        float cosTheta = 1.0f / (1.0f + m_g * m_g) * (1.0f - m_g * m_g) / (1.0f + m_g * 2.0f * sample.x() - m_g * m_g);
        float sinTheta = sqrt(1.0f - cosTheta * cosTheta);
        float phi = 2.0f * M_PI * sample.y();
        pRec.wo = Vector3f(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta);
        return 1.0f / (4 * M_PI);
    }

    virtual float pdf(const PhaseQueryRecord &pRec) const {
        return 1.0f / (4 * M_PI);
    }

    virtual std::string toString() const {
        return tfm::format(
            "Schlick[\n"
            "  g = %f"
            "]",    
            m_g
        );
    };

private:
    float m_g;
};

NORI_REGISTER_CLASS(Schlick, "schlick");
NORI_NAMESPACE_END
