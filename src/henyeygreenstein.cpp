
#include <nori/phasefunction.h>
#include <nori/common.h>

NORI_NAMESPACE_BEGIN

class HenyeyGreenstein: public PhaseFunction {
public:
    HenyeyGreenstein(const PropertyList &props) {
        m_g = props.getFloat("g", 0.0f);
    }

    virtual float sample(PhaseQueryRecord &pRec, const Point2f &sample) const {
        float a = 1 - m_g * m_g;
        float b = 1 - m_g + 2 * m_g * sample.x();
        float cosTheta = 1 / (2 * m_g) * (2 - a - (a / (b * b)));
        float sinTheta = sqrt(1 - cosTheta * cosTheta);
        float phi = 2 * M_PI * sample.y();
        pRec.wo = Vector3f(sinTheta * cos(phi), cosTheta, sinTheta * sin(phi));
        return 1.0f / (4 * M_PI);
    }

    virtual float pdf(const PhaseQueryRecord &pRec) const {
        return phaseHG(pRec.wo.dot(pRec.wi), m_g);
    }

    virtual std::string toString() const {
        return tfm::format(
            "PhaseHG[\n"
            "  g = %f"
            "]",    
            m_g
        );
    };

private:
    float m_g;
};

NORI_REGISTER_CLASS(HenyeyGreenstein, "hg");
NORI_NAMESPACE_END
