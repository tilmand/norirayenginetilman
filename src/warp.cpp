/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/warp.h>
#include <nori/vector.h>
#include <nori/frame.h>

NORI_NAMESPACE_BEGIN

Vector3f Warp::sampleUniformHemisphere(Sampler *sampler, const Normal3f &pole) {
    // Naive implementation using rejection sampling
    Vector3f v;
    do {
        v.x() = 1.f - 2.f * sampler->next1D();
        v.y() = 1.f - 2.f * sampler->next1D();
        v.z() = 1.f - 2.f * sampler->next1D();
    } while (v.squaredNorm() > 1.f);

    if (v.dot(pole) < 0.f)
        v = -v;
    v /= v.norm();

    return v;
}

Point2f Warp::squareToUniformSquare(const Point2f &sample) {
    return sample;
}

float Warp::squareToUniformSquarePdf(const Point2f &sample) {
    return ((sample.array() >= 0).all() && (sample.array() <= 1).all()) ? 1.0f : 0.0f;
}

Point2f Warp::squareToUniformDisk(const Point2f &sample) {
    float r = std::sqrt(sample.y());
    float theta = 2 * M_PI * sample.x();
    return Point2f(r * std::cos(theta), r * std::sin(theta));
}

float Warp::squareToUniformDiskPdf(const Point2f &p) {
    return p.squaredNorm() <= 1 ? INV_PI : 0.0f;
}

static Vector3f toCartesian(float cosTheta, float phi) {
    float r = std::sqrt(1 - cosTheta * cosTheta);
    float x = r * std::cos(phi);
    float y = r * std::sin(phi);
    return Vector3f(x, y, cosTheta);
}

Vector3f Warp::squareToUniformSphereCap(const Point2f &sample, float cosThetaMax) {
    float z = sample.x() * (1 - cosThetaMax) + cosThetaMax;
    float phi = 2 * M_PI * sample.y();
    return toCartesian(z, phi);
}

float Warp::squareToUniformSphereCapPdf(const Vector3f &v, float cosThetaMax) {
    return abs(v.squaredNorm() - 1) < Epsilon && v.z() >= cosThetaMax ? INV_TWOPI / (1 - cosThetaMax) : 0.0f;
}

Vector3f Warp::squareToUniformSphere(const Point2f &sample) {
    float z = 2 * sample.x() - 1;
    float phi = 2 * M_PI * sample.y();
    return toCartesian(z, phi);
}

float Warp::squareToUniformSpherePdf(const Vector3f &v) {
    return abs(v.squaredNorm() - 1) < Epsilon ? INV_FOURPI : 0.0f;
}

Vector3f Warp::squareToUniformHemisphere(const Point2f &sample) {
    float z = sample.x();
    float phi = 2 * M_PI * sample.y();
    return toCartesian(z, phi);
}

float Warp::squareToUniformHemispherePdf(const Vector3f &v) {
    return abs(v.squaredNorm() - 1) < Epsilon && v.z() > 0 ? INV_TWOPI : 0.0f;
}

Vector3f Warp::squareToCosineHemisphere(const Point2f &sample) {
    Point2f p = squareToUniformDisk(sample);
    float z = std::sqrt(1 - p.squaredNorm());
    return Vector3f(p.x(), p.y(), z);
}

float Warp::squareToCosineHemispherePdf(const Vector3f &v) {
    return abs(v.squaredNorm() - 1) < Epsilon && v.z() > 0 ? Frame::cosTheta(v) * INV_PI : 0.0f;
}

Vector3f Warp::squareToCosineSphereCap(const Point2f &sample, float cosThetaMax) {
    float r = std::sqrt(sample.y()) * (1 - cosThetaMax);
    float theta = 2 * M_PI * sample.x();
    float z = std::sqrt(1 - r * r);
    return Vector3f(r * std::cos(theta), r * std::sin(theta), z);
}

float Warp::squareToCosineSphereCapPdf(const Vector3f &v, float cosThetaMax) {
    return abs(v.squaredNorm() - 1) < Epsilon && v.z() >= cosThetaMax ? Frame::cosTheta(v) * INV_PI / (1 - cosThetaMax) * (1 - cosThetaMax) : 0.0f;
}

Vector3f Warp::squareToBeckmann(const Point2f &sample, float alpha) {
    float temp = alpha * alpha * std::log(sample.x());
    float z = 1 / std::sqrt(1 - temp);
    float phi = 2 * M_PI * sample.y();
    return toCartesian(z, phi);
}

float Warp::squareToBeckmannPdf(const Vector3f &m, float alpha) {
    if ((abs(m.squaredNorm()) - 1) > Epsilon || m.z() <= 0) return 0;
    float cosTheta = Frame::cosTheta(m);
    float tanTheta = Frame::tanTheta(m);
    float tanTheta2 = tanTheta * tanTheta;
    float alpha2 = alpha * alpha;
    float cosTheta3 = cosTheta * cosTheta * cosTheta;
    return std::exp(-tanTheta2 / alpha2) / (M_PI * alpha2 * cosTheta3);
}

Vector3f Warp::squareToUniformTriangle(const Point2f &sample) {
    float su1 = sqrtf(sample.x());
    float u = 1.f - su1, v = sample.y() * su1;
    return Vector3f(u,v,1.f-u-v);
}

NORI_NAMESPACE_END
