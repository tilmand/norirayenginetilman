/*
 Progressive photon mapper coming from the paper:
 http://graphics.ucsd.edu/~henrik/papers/progressive_photon_mapping/progressive_photon_mapping.pdf
*/

#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/emitter.h>
#include <nori/bsdf.h>
#include <nori/scene.h>
#include <nori/photon.h>
#include <thread>

NORI_NAMESPACE_BEGIN
class ProgressivePhotonMapper : public Integrator {
public:
    typedef PointKDTree<Photon> PhotonMap;
    struct {
        Point3f position;
        Normal3f normal; //Direction of the ray leading to that hitpoint
        Vector3f w;
        Intersection its; //Intersection
        float radius;
        size_t N; //Number of photons
        Color3f flux;
    } typedef HitPoint;
    //typedef PointKDTree<HitPoint> HitPointMap;

    ProgressivePhotonMapper(const PropertyList& props) {
        /* Lookup parameters */
        m_photonCount = props.getInteger("photonCount", 1000);
        m_photonRadius = props.getFloat("photonRadius", 0.05f /* Default: automatic */);
        m_photonIterations = props.getInteger("photonIterations", 10); //Can be put into props: adding photon iterations
        m_alpha = props.getFloat("alpha", 0.7f);
    }   
    virtual void preprocess(const Scene* scene) override {
        //cout << "Gathering " << m_hitPointCount<< " hitpoints .. ";
        //cout.flush();
        cout << "Photon Radius: " << m_photonRadius << "\n";
        cout.flush();

        
        /* Create a sample generator for the preprocess step */
        Sampler* sampler = static_cast<Sampler*>(
            NoriObjectFactory::createInstance("independent", PropertyList()));

        /* Estimate a default photon radius */
        if (m_photonRadius == 0)
            m_photonRadius = scene->getBoundingBox().getExtents().norm() / 500.0f;
    }
    virtual Color3f Li(const Scene* scene, Sampler* sampler, const Ray3f& _ray) const override {
        Color3f Li(0.0f), throughput(1.0f);
        Ray3f ray(_ray);
        float numLights = scene->getLights().size();

        while (true) {
            Intersection its;
            if (!scene->rayIntersect(ray, its))
                break;

            if (its.mesh->isEmitter()) {
                EmitterQueryRecord selfRec(ray.o, its.p, its.shFrame.n);
                Color3f Le = its.mesh->getEmitter()->eval(selfRec);
                Li += throughput*Le;
            }
            if (its.mesh->getBSDF()->isDiffuse()) {
                //Non-specular surface we store as hitpoint
                HitPoint hitPoint = {
                        its.p,
                        its.shFrame.n,
                        ray.d,
                        its,
                        m_photonRadius,
                        0,
                        Color3f(0.0f)
                };
                size_t N_emitted = 0;
                //Just add the luminance from the hitpoints
                for (int i = 0; i < m_photonIterations; i++) {
                    PhotonTracingPass(scene, sampler, hitPoint, N_emitted);
                }
                float numLights = scene->getLights().size();
                float scale = numLights*INV_PI / (hitPoint.radius * hitPoint.radius * N_emitted);
                Color3f ahh = hitPoint.flux * scale;
                //cout << "\n AHH: " << ahh << " This ID: " << this_id << " THis ID N_EMMITTED: " << N_emitted;
                Li +=  throughput*hitPoint.flux*scale; 
                break;
            }
            
            // Russian roulette
            float successProb = std::min(throughput.maxCoeff(), 0.99f);
            if (sampler->next1D() >= successProb)
                break;
            throughput /= successProb;

            // Sample BSDF for specular surface
            BSDFQueryRecord bRec(its.toLocal(-ray.d));
            bRec.uv = its.uv;
            bRec.p = its.p;
            Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
            if (bsdf.isZero())
                break;

            // Update throughput
            throughput *= bsdf;

            // Update ray
            ray = Ray3f(its.p, its.toWorld(bRec.wo));
        }
        return Li;
    }

    virtual std::string toString() const override {
        return tfm::format(
            "ProgressivePhotonMapper[\n"
            "  photonCount = %i,\n"
            "  photonRadius = %f\n"
            " photonIterations = %i\n"
            " alpha = %f\n"
            "]",
            m_photonCount,
            m_photonRadius,
            m_photonIterations,
            m_alpha
        );
    }
    size_t N_emitted;
private:
    int m_hitPointCount;
    size_t m_photonCount;
    float m_photonRadius;
    int m_photonIterations;
    float m_alpha;
    

    void PhotonTracingPass(const Scene* scene, Sampler* sampler, HitPoint& hitPoint, size_t& N_emitted) const {
        /* Allocate memory for the photon map */
        std::unique_ptr<PhotonMap> m_photonMap = std::unique_ptr<PhotonMap>(new PhotonMap());
        //m_photonMap->reserve(m_photonCount);

        for(int i = 0; i < m_photonCount; i++){
            Ray3f path;
            Color3f W = scene->sampleRandomPhoton(path, sampler->next1D(), sampler->next2D(), sampler->next2D());
            int burnIn = 3;
            while(true){
                Intersection its;
                if (!scene->rayIntersect(path, its))
                    break;
                if(its.mesh->getBSDF()->isDiffuse())
                    m_photonMap->push_back(Photon(its.p, -path.d, W));
                
                //Russian roulette
                float successProb = std::min(W.maxCoeff(), 0.99f);
                if (sampler->next1D() >= successProb)
                    {if(burnIn > 0)
                        burnIn--;
                    else
                        break;}
                else
                    W /= successProb;
                
                //Sample BSDF
                BSDFQueryRecord bRec(its.toLocal(-path.d));
                bRec.uv = its.uv;
                bRec.p = its.p;
                Color3f bsdf = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
                if (bsdf.isZero())
                    break;
                W *= bsdf;
                path = Ray3f(its.p, its.toWorld(bRec.wo));
            }
        }
        m_photonMap->build(true);
        N_emitted += m_photonMap->size();
        //Update hitpoint map entries
        std::vector<uint32_t> results;
        m_photonMap->search(hitPoint.position, hitPoint.radius, results);
        
        size_t M = results.size();
        
        float N_hat = hitPoint.N + m_alpha * M;
        if (N_hat == 0) {
            m_photonMap->clear();
            return;
        }
        float radius_hat = hitPoint.radius * sqrt(N_hat / (hitPoint.N + M));
        Color3f tao_n = hitPoint.flux;
        Color3f tao_m(0.0f);
        for (uint32_t res : results) {
            const Photon& photon = (*m_photonMap)[res];
            BSDFQueryRecord bRec(hitPoint.its.toLocal(-hitPoint.w), hitPoint.its.toLocal(photon.getDirection()), ESolidAngle);
            tao_m += photon.getPower()* hitPoint.its.mesh->getBSDF()->eval(bRec); //We sample the bsdf when we store the photon
        }
        Color3f tao_hat = (tao_n + tao_m) * N_hat / (hitPoint.N + M);
        hitPoint.radius = radius_hat;
        hitPoint.N = N_hat;
        hitPoint.flux = tao_hat;
    }
    
    
};

NORI_REGISTER_CLASS(ProgressivePhotonMapper, "progressivephotonmapper");
NORI_NAMESPACE_END