#include <nori/emitter.h>
#include <nori/warp.h>
#include <nori/shape.h>

NORI_NAMESPACE_BEGIN

class PointEmitter : public Emitter {
public:
    PointEmitter(const PropertyList &props) {
        m_power = props.getColor("power");
        m_intensity = m_power * INV_FOURPI;
        m_position = props.getPoint3("position");
    }

    virtual std::string toString() const override {
        return tfm::format(
                "AreaLight[\n"
                "  power = %s,\n"
                "  intensity = %s,\n"
                "  position = %s,\n"
                "]",
                m_power.toString(),
                m_intensity.toString(),
                m_position.toString());
    }

    virtual Color3f eval(const EmitterQueryRecord & lRec) const override {
        return m_intensity;
    }

    virtual Color3f sample(EmitterQueryRecord & lRec, const Point2f & sample) const override {
        lRec.wi = (m_position - lRec.ref).normalized();
        lRec.pdf = 1.f;
        lRec.p = m_position;
        lRec.n = -lRec.wi;
        lRec.shadowRay = Ray3f(lRec.ref, lRec.wi, Epsilon, (m_position - lRec.ref).norm());
        return eval(lRec) / (m_position - lRec.ref).squaredNorm();
    }

    virtual float pdf(const EmitterQueryRecord &lRec) const override {
        return 1.f;
    }


    virtual Color3f samplePhoton(Ray3f &ray, const Point2f &sample1, const Point2f &sample2) const override {
        ray = Ray3f(m_position, Warp::squareToUniformSphere(sample1));
        return m_power;
    }


protected:
    Color3f m_power;
    Color3f m_intensity;
    Point3f m_position;
};

NORI_REGISTER_CLASS(PointEmitter, "point")
NORI_NAMESPACE_END