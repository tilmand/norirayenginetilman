/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/frame.h>

NORI_NAMESPACE_BEGIN

/// Ideal dielectric BSDF
class Dielectric : public BSDF {
public:
    Dielectric(const PropertyList &propList) {
        /* Interior IOR (default: BK7 borosilicate optical glass) */
        m_intIOR = propList.getFloat("intIOR", 1.5046f);

        /* Exterior IOR (default: air) */
        m_extIOR = propList.getFloat("extIOR", 1.000277f);
    }

    virtual Color3f eval(const BSDFQueryRecord &) const override {
        /* Discrete BRDFs always evaluate to zero in Nori */
        return Color3f(0.0f);
    }

    virtual float pdf(const BSDFQueryRecord &) const override {
        /* Discrete BRDFs always evaluate to zero in Nori */
        return 0.0f;
    }

    virtual Color3f sample(BSDFQueryRecord &bRec, const Point2f &sample) const override {
        float fres = fresnel(Frame::cosTheta(bRec.wi), m_extIOR, m_intIOR);
        bool entering = Frame::cosTheta(bRec.wi) > 0;
        bRec.eta = entering ? m_extIOR / m_intIOR : m_intIOR / m_extIOR;

        if (sample.x() < fres) {
            // Reflection in local coordinates
            bRec.wo = Vector3f(
                -bRec.wi.x(),
                -bRec.wi.y(),
                 bRec.wi.z()
            );
            bRec.measure = EDiscrete;
            bRec.inside = !entering;
            return Color3f(1.0f);
        } else {
            // Refraction in local coordinates
            if (!refract(bRec)) return Color3f(0.0f); 
            bRec.measure = EDiscrete;
            bRec.inside = entering;
            return Color3f(1.0f);
        }
    }

    bool refract(BSDFQueryRecord &bRec) const {
        float cosThetaI = Frame::cosTheta(bRec.wi);
        float sin2ThetaI = Frame::sinTheta2(bRec.wi);
        float sin2ThetaT = bRec.eta * bRec.eta * sin2ThetaI;
        if (sin2ThetaT >= 1) 
            return false;
        float cosThetaT = sqrt(1 - sin2ThetaT);
        bRec.wo = Vector3f(
            bRec.eta * -bRec.wi.x(),
            bRec.eta * -bRec.wi.y(),
            (cosThetaI > 0 ? -1 : 1) * cosThetaT
        );
        return true;
    }

    virtual std::string toString() const override {
        return tfm::format(
            "Dielectric[\n"
            "  intIOR = %f,\n"
            "  extIOR = %f\n"
            "]",
            m_intIOR, m_extIOR);
    }
private:
    float m_intIOR, m_extIOR;
};

NORI_REGISTER_CLASS(Dielectric, "dielectric");
NORI_NAMESPACE_END
