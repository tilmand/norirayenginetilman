#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>
#include <nori/bsdf.h>
 
NORI_NAMESPACE_BEGIN
 
class DirectIntegrator : public Integrator {
public:
    DirectIntegrator(const PropertyList &props) { }
 
    Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
        // Find the surface that is visible in the requested direction
        Intersection its;
        if (!scene->rayIntersect(ray, its))
            return Color3f(0.0f);
 
        // Compute the direct illumination at this point
        Color3f Li(0.0f);

        // Sample a point on the light source
        EmitterQueryRecord lRec(its.p);
        std::vector<Emitter *> lights = scene->getLights();
        for (Emitter *light : lights) {
            Color3f lightColor = light->sample(lRec, sampler->next2D());
            if (lightColor.isZero())
                continue;

            // Evaluate visibility
            if (scene->rayIntersect(lRec.shadowRay))
                continue;
 
            // Evaluate the BSDF model
            BSDFQueryRecord bRec(its.toLocal(-ray.d), its.toLocal(lRec.wi), ESolidAngle);
            bRec.uv = its.uv;
            bRec.p = its.p;
            Color3f bsdf = its.mesh->getBSDF()->eval(bRec);
 
            // Add the contribution to the image
            Li += lightColor * bsdf * std::abs(Frame::cosTheta(bRec.wo));
        }

        return Li;
    }
 
    /// Return a human-readable description for debugging purposes
    std::string toString() const {
        return tfm::format(
            "DirectIntegrator"
        );
    }
};
 
NORI_REGISTER_CLASS(DirectIntegrator, "direct");
NORI_NAMESPACE_END