#include <nori/nogui.h>
#include <nori/parser.h>
#include <nori/bitmap.h>

#include <filesystem/resolver.h>

NORI_NAMESPACE_BEGIN


NoriNoScreen::NoriNoScreen(ImageBlock &block) : m_block(block), m_renderThread(m_block) {}

void NoriNoScreen::RenderXML(const std::string &filename) {

    if(m_renderThread.isBusy()) {
        cerr << "Error: rendering in progress, you need to wait until it's done" << endl;
        return;
    }

    try {
        m_renderThread.renderScene(filename);
        while (m_renderThread.isBusy()) {
            //sleep(1);
        }

    } catch (const std::exception &e) {
        cerr << "Fatal error: " << e.what() << endl;
    }

}

NORI_NAMESPACE_END
