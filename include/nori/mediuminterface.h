#if !defined(__NORI_MEDIUMINTERFACE_H)
#define __NORI_MEDIUMINTERFACE_H

#include <nori/medium.h>

NORI_NAMESPACE_BEGIN

class MediumInterface
{
public:
    MediumInterface(const Medium *medium = nullptr)
        : inside(medium), outside(medium) { }

    MediumInterface(const Medium *inside, const Medium *outside)
        : inside(inside), outside(outside) { }

    bool isMediumTransition() const {
        return inside != outside;
    }

    const Medium *inside, *outside;
};

NORI_NAMESPACE_END

#endif /* __NORI_MEDIUMINTERFACE_H */