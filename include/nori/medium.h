#if !defined(__NORI_MEDIUM_H)
#define __NORI_MEDIUM_H

#include <nori/object.h>
#include <nori/phasefunction.h>

NORI_NAMESPACE_BEGIN

class Medium : public NoriObject {
public:
    virtual Color3f evalTransmittance(const Ray3f &ray) const = 0;

    virtual Color3f evalAbsorption(const Ray3f &ray) const = 0;

    virtual Color3f evalScattering(const Ray3f &ray) const = 0;

    virtual Color3f evalExtinction(const Ray3f &ray) const = 0;

    virtual Color3f evalAlbedo(const Ray3f &ray) const = 0;

    virtual Color3f evalEmission(const Ray3f &ray) const = 0;

    virtual Color3f evalPhase(const Vector3f &wi, const Vector3f &wo) const {
        if (m_phaseFunction) {
            PhaseQueryRecord pRec(wi, wo);
            return Color3f(m_phaseFunction->pdf(pRec));
        } else
            return Color3f(1.0f);
    }

    virtual Color3f samplePhase(const Vector3f &wo, Vector3f &wi, const Point2f &sample) const {
        if (m_phaseFunction) {
            PhaseQueryRecord pRec(wo);
            float pdf = m_phaseFunction->sample(pRec, sample);
            wi = pRec.wo;
            return Color3f(pdf);
        } else {
            wi = wo;
            return Color3f(1.0f);
        }
    }

    virtual float pdf(const Vector3f &wo, const Vector3f &wi) const = 0;

    virtual EClassType getClassType() const override { return EMedium; }

    virtual void addChild(NoriObject *obj) override {
        switch (obj->getClassType()) {
            case EPhaseFunction:
                m_phaseFunction = static_cast<PhaseFunction *>(obj);
                break;
            default:
                throw NoriException("Medium::addChild(<%s>) is not supported!",
                    classTypeName(obj->getClassType()));
        }
    }

protected:
    PhaseFunction *m_phaseFunction = nullptr;
};

NORI_NAMESPACE_END

#endif /* __NORI_MEDIUM_H */
