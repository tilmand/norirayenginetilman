#if !defined(__NORI_PHASE_H)
#define __NORI_PHASE_H

#include <nori/object.h>

NORI_NAMESPACE_BEGIN

struct PhaseQueryRecord
{
    PhaseQueryRecord(const Vector3f &wi) 
        : wi(wi) { }

    PhaseQueryRecord(const Vector3f &wi, const Vector3f &wo)
        : wi(wi), wo(wo) { }

    Vector3f wi, wo;
};

class PhaseFunction : public NoriObject
{
public:
    virtual float sample(PhaseQueryRecord &pRec, const Point2f &sample) const = 0;

    virtual float pdf(const PhaseQueryRecord &pRec) const = 0;

    virtual EClassType getClassType() const override { return EPhaseFunction; }
};

NORI_NAMESPACE_END

#endif /* __NORI_PHASE_H */
