#include <nori/common.h>
#include <nori/render.h>

NORI_NAMESPACE_BEGIN

class NoriNoScreen {
public:
    NoriNoScreen(ImageBlock &block);

    void RenderXML(const std::string & filename);

private:
    ImageBlock &m_block;
    RenderThread m_renderThread;
};

NORI_NAMESPACE_END
