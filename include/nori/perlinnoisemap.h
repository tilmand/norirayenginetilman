#if !defined(NORI_PERLINNOISEMAP_H_)
#define NORI_PERLINNOISEMAP_H_

#include <math.h>
#include <hash_map>
#include <nori/object.h>

NORI_NAMESPACE_BEGIN

class PerlinNoiseMap {
public:

    PerlinNoiseMap(float grid_size) : m_grid_size(grid_size) {}

    /* Function to linearly interpolate between a0 and a1
    * Weight w should be in the range [0.0, 1.0]
    */
    float interpolate(float a0, float a1, float w) {
        /* // You may want clamping by inserting:
        * if (0.0 > w) return a0;
        * if (1.0 < w) return a1;
        */
        return (a1 - a0) * w + a0;
        /* // Use this cubic interpolation [[Smoothstep]] instead, for a smooth appearance:
        * return (a1 - a0) * (3.0 - w * 2.0) * w * w + a0;
        *
        * // Use [[Smootherstep]] for an even smoother result with a second derivative equal to zero on boundaries:
        * return (a1 - a0) * ((w * (w * 6.0 - 15.0) + 10.0) * w * w * w) + a0;
        */
    }

    /* Create pseudorandom direction vector
    */
    Vector3f randomGradient(float ix, float iy, float iz) {
        // Use std::hash to get two random numbers from the seed
        std::hash<float> hash;
        float random1 = hash(ix + hash(iy + hash(iz)));
        float random2 = hash(ix + hash(iy + hash(iz + 1)));

        // Generate a pseudorandom, noramlized vector with an even distribution in all directions
        float angle = 2.0f * M_PI * random1;
        float z = 1.0f - 2.0f * random2;
        float r = sqrtf(1.0f - z * z);
        float x = r * cosf(angle);
        float y = r * sinf(angle);
        return Vector3f(x, y, z);
    }

    // Computes the dot product of the distance and gradient vectors.
    float dotGridGradient(float ix, float iy, float iz,float x, float y, float z) {
        // Get gradient from integer coordinates
        Vector3f gradient = randomGradient(ix, iy, iz);

        // Compute the distance vector
        float dx = x - ix;
        float dy = y - iy;
        float dz = z - iz;

        // Compute the dot-product
        return (dx * gradient[0] + dy * gradient[1] + dz * gradient[2]);
    }

    // Compute Perlin noise at coordinates x, y
    float sample(Point3f p) {
        // Determine grid cell coordinates
        float x = p[0];
        float y = p[1];
        float z = p[2];

        // Get closest point on grid with grip spacing of m_grid_size
        float x0 = floor(x / m_grid_size) * m_grid_size;
        float x1 = x0 + m_grid_size;
        float y0 = floor(y / m_grid_size) * m_grid_size;
        float y1 = y0 + m_grid_size;
        float z0 = floor(z / m_grid_size) * m_grid_size;
        float z1 = z0 + m_grid_size;


        // Determine interpolation weights
        // Could also use higher order polynomial/s-curve here
        float sx = x - x0;
        float sy = y - y0;
        float sz = z - z0;

        // Interpolate between grid point gradients
        float n0, n1, ix0, ix1, ix2, ix3, ix4, ix5, value;

        n0 = dotGridGradient(x0, y0, z0, x, y, z);
        n1 = dotGridGradient(x1, y0, z0, x, y, z);
        ix0 = interpolate(n0, n1, sx);

        n0 = dotGridGradient(x0, y1, z0, x, y, z);
        n1 = dotGridGradient(x1, y1, z0, x, y, z);
        ix1 = interpolate(n0, n1, sx);

        n0 = dotGridGradient(x0, y0, z1, x, y, z);
        n1 = dotGridGradient(x1, y0, z1, x, y, z);
        ix2 = interpolate(n0, n1, sx);

        n0 = dotGridGradient(x0, y1, z1, x, y, z);
        n1 = dotGridGradient(x1, y1, z1, x, y, z);
        ix3 = interpolate(n0, n1, sx);

        ix4 = interpolate(ix0, ix1, sy);
        ix5 = interpolate(ix2, ix3, sy);

        value = interpolate(ix4, ix5, sy);
        return value; // Will return in range -1 to 1. To make it in range 0 to 1, multiply by 0.5 and add 0.5
    }

protected:
    float m_grid_size;
};

NORI_NAMESPACE_END

#endif /* !defined(NORI_PERLINNOISEMAP_H_) */